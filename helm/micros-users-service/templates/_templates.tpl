{{- define "micros.deployment" }}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .appName }}-deployment
  namespace: {{ .namespace }}
  labels:
    app: {{ .appName }}
spec:
  replicas: {{ .replicasCount }}
  selector:
    matchLabels:
      app: {{ .appName }}
  template:
    metadata:
      labels:
        app: {{ .appName }}
    spec:
      containers:
      - name: {{ .appName }}
        image: {{ .container.image }}:{{ .container.tag }}
        ports: 
        - containerPort: {{ .container.port }}
        readinessProbe:
          httpGet:
            path: /actuator/health
            port: {{ .container.port }}
          initialDelaySeconds: 10
          periodSeconds: 10
          failureThreshold: 20
        resources:
          requests:
            memory: 256M
            cpu: 100m
          limits:
            memory: 512M
            cpu: 200m
        env:
{{ toYaml .container.env | indent 8}}
{{- end }}


{{- define "micros.service" }}
apiVersion: v1
kind: Service
metadata:
  name: {{ .appName }}
  namespace: {{ .namespace }}
spec:
  selector:
    app: {{ .appName }}
  type: {{ .service.type }}
  ports:
  - protocol: TCP
    port: {{ .service.port }}
    targetPort: {{ .service.targetPort }}
    {{- if .nodePort}}nodePort: {{ .service.nodePort }}{{- end }}
{{- end }}