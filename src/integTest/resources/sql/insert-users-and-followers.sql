INSERT INTO users_schema.users(user_id, first_name, last_name, email, phone, soft_deleted) VALUES ('3fa85f64-5717-4562-b3fc-2c963f66afa3', 'test3', 'last3', 'email3', 'phone3', false);
INSERT INTO users_schema.users(user_id, first_name, last_name, email, phone, soft_deleted) VALUES ('3fa85f64-5717-4562-b3fc-2c963f66afa4', 'test4', 'last4', 'email4', 'phone4', false);
INSERT INTO users_schema.users(user_id, first_name, last_name, email, phone, soft_deleted) VALUES ('3fa85f64-5717-4562-b3fc-2c963f66afa5', 'test5', 'last5', 'email5', 'phone5', false);

INSERT INTO users_schema.followers(user_id, follower_id) VALUES ('3fa85f64-5717-4562-b3fc-2c963f66afa3', '3fa85f64-5717-4562-b3fc-2c963f66afa4');
INSERT INTO users_schema.followers(user_id, follower_id) VALUES ('3fa85f64-5717-4562-b3fc-2c963f66afa3', '3fa85f64-5717-4562-b3fc-2c963f66afa5');