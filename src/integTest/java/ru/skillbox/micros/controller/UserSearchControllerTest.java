package ru.skillbox.micros.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.transaction.Transactional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@Transactional
public class UserSearchControllerTest {

  @Autowired
  MockMvc mockMvc;

  @Test
  @Sql({"classpath:/sql/insert-two-users.sql"})
  void shouldReturnAllUsers() throws Exception {
    mockMvc.perform(get("/api/v1/users/").contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$[0].userId").value("3fa85f64-5717-4562-b3fc-2c963f66afa6"))
        .andExpect(jsonPath("$[0].firstName").value("test1"))
        .andExpect(jsonPath("$[0].lastName").value("last1"))
        .andExpect(jsonPath("$[0].email").value("email1"))
        .andExpect(jsonPath("$[0].phone").value("phone1"))
        .andExpect(jsonPath("$[1].userId").value("3fa85f64-5717-4562-b3fc-2c963f66afa7"))
        .andExpect(jsonPath("$[1].firstName").value("test2"))
        .andExpect(jsonPath("$[1].lastName").value("last2"))
        .andExpect(jsonPath("$[1].email").value("email2"))
        .andExpect(jsonPath("$[1].phone").value("phone2"));
  }

}