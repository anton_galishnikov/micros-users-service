package ru.skillbox.micros.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.UUID;
import javax.transaction.Transactional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import ru.skillbox.micros.model.dto.UserDto;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@Transactional
public class UserControllerTest {

  @Autowired
  MockMvc mockMvc;

  @Autowired
  ObjectMapper objectMapper;

  @Autowired
  JdbcTemplate jdbc;

  @Test
  void createShouldPersistUser() throws Exception {
    UserDto user = new UserDto(UUID.randomUUID(), "first", "last", "email", "phone");
    mockMvc.perform(post("/api/v1/users").contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(user)))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.userId").isNotEmpty())
        .andExpect(jsonPath("$.firstName").value("first"))
        .andExpect(jsonPath("$.lastName").value("last"))
        .andExpect(jsonPath("$.email").value("email"))
        .andExpect(jsonPath("$.phone").value("phone"));
  }

  @Test
  @Sql({"classpath:/sql/insert-two-users.sql"})
  void getShouldReturnUserWhenFound() throws Exception {
    mockMvc.perform(get("/api/v1/users/3fa85f64-5717-4562-b3fc-2c963f66afa6").contentType(
                MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.userId").value("3fa85f64-5717-4562-b3fc-2c963f66afa6"))
        .andExpect(jsonPath("$.firstName").value("test1"))
        .andExpect(jsonPath("$.lastName").value("last1"))
        .andExpect(jsonPath("$.email").value("email1"))
        .andExpect(jsonPath("$.phone").value("phone1"));
  }

  @Test
  @Sql({"classpath:/sql/insert-two-users.sql"})
  void getShouldReturn404WhenNotFound() throws Exception {
    mockMvc.perform(get("/api/v1/users/3fa85f64-5717-4562-b3fc-2c963f66afa8").contentType(
                MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound());
  }

  @Test
  @Sql({"classpath:/sql/insert-two-users.sql"})
  void deleteShouldRemoveUserWhenPresent() throws Exception {
    UUID id = UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa6");
    String countQuery = "select count(1) from users_schema.users where user_id = ? and soft_deleted = false";

    Integer initialCount = jdbc.queryForObject(countQuery, Integer.class, id);
    assertEquals(1, initialCount);

    mockMvc.perform(delete("/api/v1/users/%s".formatted(id)).contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());

    Integer actualCount = jdbc.queryForObject(countQuery, Integer.class, id);
    assertEquals(0, actualCount);
  }

  @Test
  @Sql({"classpath:/sql/insert-two-users.sql"})
  void deleteShouldReturn404WhenNotPresent() throws Exception {
    UUID id = UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa8");
    mockMvc.perform(delete("/api/v1/users/%s".formatted(id)).contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound());
  }

  @Test
  @Sql({"classpath:/sql/insert-two-users.sql"})
  void putShouldWorkForNewUser() throws Exception {
    UUID id = UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa8");
    UserDto user = new UserDto(id, "one", "two", "three", "four");
    mockMvc.perform(put("/api/v1/users/%s".formatted(id)).contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(user)))
        .andExpect(status().isOk());
  }

  @Test
  @Sql({"classpath:/sql/insert-two-users.sql"})
  void putShouldWorkForExistingUser() throws Exception {
    UUID id = UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa6");
    UserDto user = new UserDto(id, "one", "two", "three", "four");
    mockMvc.perform(put("/api/v1/users/%s".formatted(id)).contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(user)))
        .andExpect(status().isOk());
  }

  @Test
  @Sql({"classpath:/sql/insert-two-users.sql"})
  void putShouldReturnBadRequestWhenIdMismatch() throws Exception {
    UUID id1 = UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa6");
    UUID id2 = UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa7");
    UserDto user = new UserDto(id1, "one", "two", "three", "four");
    mockMvc.perform(put("/api/v1/users/%s".formatted(id2)).contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(user)))
        .andExpect(status().isBadRequest());
  }
}