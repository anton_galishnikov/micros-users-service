package ru.skillbox.micros.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.UUID;
import javax.transaction.Transactional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@Transactional
public class UserFollowingControllerTest {

  @Autowired
  MockMvc mockMvc;

  @Test
  @Sql({"classpath:/sql/insert-two-users.sql"})
  void getFollowShouldReturn404WhenNoUser() throws Exception {
    UUID doesNotExist = UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa8");
    mockMvc.perform(get("/api/v1/users/%s/follow".formatted(doesNotExist)).contentType(
                MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound());
  }

  @Test
  @Sql({"classpath:/sql/insert-users-and-followers.sql"})
  void getFollowShouldReturnFollowList() throws Exception {
    UUID id = UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa3");
    mockMvc.perform(
            get("/api/v1/users/%s/follow".formatted(id)).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$[0].userId").value("3fa85f64-5717-4562-b3fc-2c963f66afa4"))
        .andExpect(jsonPath("$[1].userId").value("3fa85f64-5717-4562-b3fc-2c963f66afa5"))
        .andExpect(jsonPath("$", hasSize(2)));
  }

  @Test
  @Sql({"classpath:/sql/insert-two-users.sql"})
  void getFollowerShouldReturn404WhenNoUser() throws Exception {
    UUID doesNotExist = UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa8");
    mockMvc.perform(get("/api/v1/users/%s/follower".formatted(doesNotExist)).contentType(
                MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound());
  }

  @Test
  @Sql({"classpath:/sql/insert-users-and-followers.sql"})
  void getFollowerShouldReturnFollowerList() throws Exception {
    UUID id = UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa4");
    mockMvc.perform(
            get("/api/v1/users/%s/follower".formatted(id)).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$[0].userId").value("3fa85f64-5717-4562-b3fc-2c963f66afa3"));
  }

  @Test
  @Sql({"classpath:/sql/insert-two-users.sql"})
  void postFollowShouldAddUserToFollowList() throws Exception {
    UUID id1 = UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa6");
    UUID id2 = UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa7");
    mockMvc.perform(post("/api/v1/users/%s/follow/%s".formatted(id1, id2)).contentType(
                MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$[0].userId").value(id2.toString()));
  }

  @Test
  @Sql({"classpath:/sql/insert-two-users.sql"})
  void postFollowShouldReturnBadRequestWhenFollowSelf() throws Exception {
    UUID id = UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa6");
    mockMvc.perform(
            post("/api/v1/users/%s/follow/%s".formatted(id, id)).contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest());
  }

  @Test
  @Sql({"classpath:/sql/insert-two-users.sql"})
  void postFollowShould404WhenFirstArgNotFound() throws Exception {
    UUID exists = UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa6");
    UUID notExists = UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa8");
    mockMvc.perform(post("/api/v1/users/%s/follow/%s".formatted(exists, notExists)).contentType(
                MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound());
  }

  @Test
  @Sql({"classpath:/sql/insert-two-users.sql"})
  void postFollowShould404WhenSecondArgNotFound() throws Exception {
    UUID exists = UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa6");
    UUID notExists = UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa8");
    mockMvc.perform(post("/api/v1/users/%s/follow/%s".formatted(notExists, exists)).contentType(
                MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound());
  }

  @Test
  @Sql({"classpath:/sql/insert-two-users.sql"})
  void deleteFollowShould404WhenFirstArgNotFound() throws Exception {
    UUID exists = UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa6");
    UUID notExists = UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa8");
    mockMvc.perform(delete("/api/v1/users/%s/follow/%s".formatted(exists, notExists)).contentType(
                MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound());
  }

  @Test
  @Sql({"classpath:/sql/insert-two-users.sql"})
  void deleteFollowShould404WhenSecondArgNotFound() throws Exception {
    UUID exists = UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa6");
    UUID notExists = UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa8");
    mockMvc.perform(delete("/api/v1/users/%s/follow/%s".formatted(notExists, exists)).contentType(
                MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound());
  }

  @Test
  @Sql({"classpath:/sql/insert-users-and-followers.sql"})
  void deleteFollowShouldRemoveFromFollowList() throws Exception {
    UUID id1 = UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa3");
    UUID id2 = UUID.fromString("3fa85f64-5717-4562-b3fc-2c963f66afa4");
    mockMvc.perform(delete("/api/v1/users/%s/follow/%s".formatted(id1, id2)).contentType(
                MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$[0].userId").value("3fa85f64-5717-4562-b3fc-2c963f66afa5"))
        .andExpect(jsonPath("$", hasSize(1)));
  }

}