package ru.skillbox.micros.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;
import ru.skillbox.micros.model.entity.User;
import ru.skillbox.micros.repository.UserRepository;

public class UserFollowingServiceImplTest {

  UserRepository repository = mock(UserRepository.class);
  UserFollowingServiceImpl service = new UserFollowingServiceImpl(repository);

  @Test
  public void followListShouldReturnFollowListFromEntity() {
    User user = new User();
    UUID id = UUID.randomUUID();
    user.setUserId(id);
    List<User> expected = List.of(new User(), new User());
    user.setFollowList(expected);
    when(repository.findById(eq(id))).thenReturn(Optional.of(user));

    List<User> actual = service.followList(id);

    assertIterableEquals(expected, actual);
  }

  @Test
  public void followerListShouldReturnFollowerListFromEntity() {
    User user = new User();
    UUID id = UUID.randomUUID();
    user.setUserId(id);
    List<User> expected = List.of(new User(), new User());
    user.setFollowerList(expected);
    when(repository.findById(eq(id))).thenReturn(Optional.of(user));

    List<User> actual = service.followerList(id);

    assertIterableEquals(expected, actual);
  }

  @Test
  public void followShouldThrowExceptionWhenTryingToFollowSelf() {
    UUID id = UUID.randomUUID();

    ResponseStatusException actual = assertThrows(ResponseStatusException.class, () -> {
      service.follow(id, id);
    });

    assertEquals(HttpStatus.BAD_REQUEST, actual.getStatus());
  }

  @Test
  public void followShouldAddToFollowList() {
    UUID id1 = UUID.randomUUID();
    User user1 = new User();
    user1.setUserId(id1);

    User followed1 = new User();
    followed1.setUserId(UUID.randomUUID());
    User followed2 = new User();
    followed2.setUserId(UUID.randomUUID());
    List<User> currentFollowList = new ArrayList<>(List.of(followed1, followed2));
    user1.setFollowList(currentFollowList);

    when(repository.findById(eq(id1))).thenReturn(Optional.of(user1));

    UUID id2 = UUID.randomUUID();
    User user2 = new User();
    user2.setUserId(id2);
    when(repository.findById(eq(id2))).thenReturn(Optional.of(user2));

    List<User> actual = service.follow(id1, id2);

    assertIterableEquals(List.of(followed1, followed2, user2), actual);
  }

  @Test
  public void unfollowShouldRemoveFromFollowList() {
    UUID id2 = UUID.randomUUID();
    User user2 = new User();
    user2.setUserId(id2);
    when(repository.findById(eq(id2))).thenReturn(Optional.of(user2));

    UUID id1 = UUID.randomUUID();
    User user1 = new User();
    user1.setUserId(id1);

    User followed = new User();
    followed.setUserId(UUID.randomUUID());
    List<User> currentFollowList = new ArrayList<>(List.of(followed, user2));
    user1.setFollowList(currentFollowList);
    when(repository.findById(eq(id1))).thenReturn(Optional.of(user1));

    List<User> actual = service.unfollow(id1, id2);

    assertIterableEquals(List.of(followed), actual);
  }

  @Test
  public void followShouldThrowExceptionWhenUserNotFound() {
    testThrowsNotFound(() -> service.follow(UUID.randomUUID(), UUID.randomUUID()));
  }

  @Test
  public void unfollowShouldThrowExceptionWhenUserNotFound() {
    testThrowsNotFound(() -> service.unfollow(UUID.randomUUID(), UUID.randomUUID()));
  }

  @Test
  public void followListShouldThrowExceptionWhenUserNotFound() {
    testThrowsNotFound(() -> service.followList(UUID.randomUUID()));
  }

  @Test
  public void followerListShouldThrowExceptionWhenUserNotFound() {
    testThrowsNotFound(() -> service.followerList(UUID.randomUUID()));
  }

  private void testThrowsNotFound(Executable executable) {
    when(repository.findById(any())).thenReturn(Optional.empty());

    ResponseStatusException actual = assertThrows(ResponseStatusException.class, executable);

    assertEquals(HttpStatus.NOT_FOUND, actual.getStatus());
  }
}