package ru.skillbox.micros.service;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;
import org.junit.jupiter.api.Test;
import ru.skillbox.micros.model.entity.User;
import ru.skillbox.micros.repository.UserRepository;

public class UserSearchServiceImplTest {

  UserRepository repository = mock(UserRepository.class);
  UserSearchServiceImpl service = new UserSearchServiceImpl(repository);

  @Test
  public void allShouldReturnAllUsers() {
    List<User> expected = List.of(new User(), new User(), new User());
    when(repository.findAll()).thenReturn(expected);

    List<User> actual = service.all();

    assertIterableEquals(expected, actual);
  }

}