package ru.skillbox.micros.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;
import ru.skillbox.micros.model.entity.User;
import ru.skillbox.micros.repository.UserRepository;

public class UserServiceImplTest {

  UserRepository repository = mock(UserRepository.class);
  UserServiceImpl service = new UserServiceImpl(repository);

  @Test
  public void readShouldReturnEntityWhenEntityFound() {
    UUID id = UUID.randomUUID();
    User expected = new User();
    expected.setUserId(id);
    when(repository.findById(eq(id))).thenReturn(Optional.of(expected));

    User actual = service.read(id);

    assertEquals(expected, actual);
  }

  @Test
  public void readShouldThrowExceptionWhenEntityNotFound() {
    UUID id = UUID.randomUUID();
    when(repository.findById(eq(id))).thenReturn(Optional.empty());

    ResponseStatusException actual = assertThrows(ResponseStatusException.class, () -> {
      service.read(id);
    });

    assertEquals(HttpStatus.NOT_FOUND, actual.getStatus());
  }

  @Test
  public void createShouldReturnSavedEntity() {
    UUID id = UUID.randomUUID();
    User input = new User();
    User expected = new User();
    expected.setUserId(id);
    when(repository.saveAndFlush(eq(input))).thenReturn(expected);

    User actual = service.create(input);

    assertEquals(expected, actual);
  }

  @Test
  public void updateShouldThrowExceptionWhenDifferentIds() {
    UUID id1 = UUID.randomUUID();
    UUID id2 = UUID.randomUUID();
    User user = new User();
    user.setUserId(id1);

    ResponseStatusException actual = assertThrows(ResponseStatusException.class, () -> {
      service.update(id2, user);
    });

    assertEquals(HttpStatus.BAD_REQUEST, actual.getStatus());
  }

  @Test
  public void updateShouldReturnSavedUser() {
    UUID id = UUID.randomUUID();
    User expected = new User();
    expected.setUserId(id);
    when(repository.saveAndFlush(eq(expected))).thenReturn(expected);

    User actual = service.update(id, expected);

    assertEquals(expected, actual);
  }

  @Test
  public void deleteShouldDeleteWhenUserFound() {
    UUID id = UUID.randomUUID();
    User user = new User();
    user.setUserId(id);
    when(repository.findById(eq(id))).thenReturn(Optional.of(user));

    service.delete(id);

    verify(repository, times(1)).delete(eq(user));
  }

  @Test
  public void deleteShouldThrowExceptionWhenNotFound() {
    UUID id = UUID.randomUUID();
    when(repository.findById(eq(id))).thenReturn(Optional.empty());

    ResponseStatusException actual = assertThrows(ResponseStatusException.class, () -> {
      service.delete(id);
    });

    assertEquals(HttpStatus.NOT_FOUND, actual.getStatus());
  }

}