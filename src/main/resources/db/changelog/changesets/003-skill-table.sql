CREATE TABLE users_schema.skills(
    skill_id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
    name VARCHAR NOT NULL,
    type VARCHAR NOT NULL,
    soft_deleted BOOLEAN DEFAULT FALSE NOT NULL
);

CREATE TABLE users_schema.user_skills(
    user_id uuid,
    skill_id uuid,
    PRIMARY KEY(user_id, skill_id),
    CONSTRAINT fk_user FOREIGN KEY(user_id) REFERENCES users_schema.users(user_id),
    CONSTRAINT fk_skill FOREIGN KEY(skill_id) REFERENCES users_schema.skills(skill_id)
)
