
CREATE TABLE users_schema.cities(
    city_id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
    name VARCHAR,
    soft_deleted BOOLEAN DEFAULT FALSE NOT NULL
);

CREATE TABLE users_schema.users(
    user_id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
    first_name VARCHAR NOT NULL,
    last_name VARCHAR NOT NULL,
    patronymic VARCHAR,
    gender VARCHAR,
    birthday DATE,
    city_id uuid,
    avatar VARCHAR,
    bio VARCHAR,
    nickname VARCHAR,
    email VARCHAR NOT NULL,
    phone VARCHAR NOT NULL,
    soft_deleted BOOLEAN DEFAULT FALSE NOT NULL,
    CONSTRAINT fk_city FOREIGN KEY(city_id) REFERENCES users_schema.cities(city_id)
);

CREATE TABLE users_schema.followers(
    user_id uuid,
    follower_id uuid,
    PRIMARY KEY(user_id, follower_id),
    CONSTRAINT fk_user FOREIGN KEY(user_id) REFERENCES users_schema.users(user_id),
    CONSTRAINT fk_follower FOREIGN KEY(follower_id) REFERENCES users_schema.users(user_id)
)