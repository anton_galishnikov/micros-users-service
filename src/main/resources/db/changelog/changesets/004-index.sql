CREATE INDEX users_by_gender_idx ON users_schema.users(gender);
CREATE INDEX users_by_city_idx ON users_schema.users(city_id);
CREATE INDEX users_by_gender_and_city_idx ON users_schema.users(gender, city_id);

CREATE INDEX followers_by_user_idx ON users_schema.followers(user_id);
CREATE INDEX followers_by_follower_idx ON users_schema.followers(follower_id);

CREATE INDEX user_skills_by_user_idx ON users_schema.user_skills(user_id);
CREATE INDEX user_skills_by_skill_idx ON users_schema.user_skills(user_id);