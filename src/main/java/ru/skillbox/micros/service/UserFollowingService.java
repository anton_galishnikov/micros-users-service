package ru.skillbox.micros.service;

import java.util.List;
import java.util.UUID;
import ru.skillbox.micros.model.entity.User;

public interface UserFollowingService {

  List<User> followList(UUID id);

  List<User> follow(UUID id, UUID followId);

  List<User> unfollow(UUID id, UUID followId);

  List<User> followerList(UUID id);

}
