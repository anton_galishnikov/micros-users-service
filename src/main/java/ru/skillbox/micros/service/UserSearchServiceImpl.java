package ru.skillbox.micros.service;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.skillbox.micros.model.entity.User;
import ru.skillbox.micros.repository.UserRepository;

@RequiredArgsConstructor
@Service
public class UserSearchServiceImpl implements UserSearchService {

  private final UserRepository repository;

  @Override
  public List<User> all() {
    return repository.findAll();
  }

}
