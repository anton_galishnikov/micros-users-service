package ru.skillbox.micros.service;

import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.skillbox.micros.model.entity.User;
import ru.skillbox.micros.repository.UserRepository;

@RequiredArgsConstructor
@Service
public class UserFollowingServiceImpl implements UserFollowingService {

  private final UserRepository repository;

  @Override
  public List<User> followList(UUID id) {
    User user = read(id);
    return user.getFollowList();
  }

  @Override
  public List<User> follow(UUID id, UUID followId) {
    if (id.equals(followId)) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User can't subscribe to self");
    }
    User user = read(id);
    User followUser = read(followId);
    List<User> followList = user.getFollowList();
    followList.add(followUser);
    repository.saveAndFlush(user);
    return followList;
  }

  @Override
  public List<User> unfollow(UUID id, UUID followId) {
    User user = read(id);
    User followUser = read(followId);
    List<User> followList = user.getFollowList();
    followList.remove(followUser);
    repository.saveAndFlush(user);
    return followList;
  }

  @Override
  public List<User> followerList(UUID id) {
    User user = read(id);
    return user.getFollowerList();
  }

  private User read(UUID id) {
    return repository.findById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
            "No user found with id %s".formatted(id)));
  }
}
