package ru.skillbox.micros.service;

import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import ru.skillbox.micros.model.entity.User;
import ru.skillbox.micros.repository.UserRepository;

@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService {

  private final UserRepository repository;

  @Override
  public User read(UUID id) {
    return repository.findById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
            "No user found with id %s".formatted(id)));
  }

  @Override
  public User create(User user) {
    return repository.saveAndFlush(user);
  }

  @Override
  public User update(UUID id, User user) {
    if (!id.equals(user.getUserId())) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
          "Id in path doesn't match id in body");
    }

    return repository.saveAndFlush(user);
  }

  @Override
  public void delete(UUID id) {
    User user = repository.findById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    repository.delete(user);
    repository.flush();
  }
}
