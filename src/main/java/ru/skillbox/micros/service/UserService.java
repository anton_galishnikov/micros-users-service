package ru.skillbox.micros.service;

import java.util.UUID;
import ru.skillbox.micros.model.entity.User;

public interface UserService {

  User read(UUID id);

  User create(User user);

  User update(UUID id, User user);

  void delete(UUID id);

}
