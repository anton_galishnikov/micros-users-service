package ru.skillbox.micros.service;

import java.util.List;
import ru.skillbox.micros.model.entity.User;

public interface UserSearchService {

  List<User> all();
}
