package ru.skillbox.micros.repository;

import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.skillbox.micros.model.entity.User;

public interface UserRepository extends JpaRepository<User, UUID> {

}
