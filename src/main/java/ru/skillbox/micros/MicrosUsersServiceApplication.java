package ru.skillbox.micros;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicrosUsersServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(MicrosUsersServiceApplication.class, args);
  }

}
