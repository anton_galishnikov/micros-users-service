package ru.skillbox.micros.controller;

import static org.springframework.http.ResponseEntity.ok;

import io.swagger.v3.oas.annotations.Operation;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.skillbox.micros.model.dto.UserDto;
import ru.skillbox.micros.model.entity.User;
import ru.skillbox.micros.model.mapper.UserMapper;
import ru.skillbox.micros.service.UserService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/users")
@SuppressWarnings("unused")
public class UserController {

  private final UserService service;
  private final UserMapper mapper;

  @GetMapping("/{id}")
  @Operation(description = "Get user")
  public ResponseEntity<UserDto> get(@PathVariable UUID id) {
    User user = service.read(id);
    return ok(mapper.toDto(user));
  }

  @PostMapping
  @Operation(description = "Create user")
  public ResponseEntity<UserDto> post(@RequestBody UserDto user) {
    User savedUser = service.create(mapper.toEntity(user));
    return ok(mapper.toDto(savedUser));
  }

  @PutMapping("/{id}")
  @Operation(description = "Update user")
  public ResponseEntity<UserDto> put(@PathVariable UUID id, @RequestBody UserDto user) {
    User updatedUser = service.update(id, mapper.toEntity(user));
    return ok(mapper.toDto(updatedUser));
  }

  @DeleteMapping("/{id}")
  @Operation(description = "Delete user")
  public ResponseEntity<Void> delete(@PathVariable UUID id) {
    service.delete(id);
    return ok().build();
  }

}
