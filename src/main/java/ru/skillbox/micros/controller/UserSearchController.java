package ru.skillbox.micros.controller;

import static org.springframework.http.ResponseEntity.ok;

import io.swagger.v3.oas.annotations.Operation;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.skillbox.micros.model.dto.UserDto;
import ru.skillbox.micros.model.entity.User;
import ru.skillbox.micros.model.mapper.UserMapper;
import ru.skillbox.micros.service.UserSearchService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/users")
@SuppressWarnings("unused")
public class UserSearchController {

  private final UserSearchService service;
  private final UserMapper mapper;

  @GetMapping
  @Operation(description = "Get all users")
  public ResponseEntity<List<UserDto>> get() {
    List<User> allUsers = service.all();
    return ok(mapper.toDto(allUsers));
  }
}
