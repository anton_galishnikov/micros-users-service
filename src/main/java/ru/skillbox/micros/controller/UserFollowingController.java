package ru.skillbox.micros.controller;

import static org.springframework.http.ResponseEntity.ok;

import io.swagger.v3.oas.annotations.Operation;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.skillbox.micros.model.dto.UserDto;
import ru.skillbox.micros.model.entity.User;
import ru.skillbox.micros.model.mapper.UserMapper;
import ru.skillbox.micros.service.UserFollowingService;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/users")
@SuppressWarnings("unused")
public class UserFollowingController {

  private final UserFollowingService service;
  private final UserMapper mapper;

  @GetMapping("/{id}/follow")
  @Operation(description = "Get user's follow list")
  public ResponseEntity<List<UserDto>> followList(@PathVariable UUID id) {
    List<User> followList = service.followList(id);
    return ok(mapper.toDto(followList));
  }

  @PostMapping("/{id}/follow/{followId}")
  @Operation(description = "Add user with followId to the follow list")
  public ResponseEntity<List<UserDto>> follow(@PathVariable UUID id, @PathVariable UUID followId) {
    List<User> followList = service.follow(id, followId);
    return ok(mapper.toDto(followList));
  }

  @DeleteMapping("/{id}/follow/{followId}")
  @Operation(description = "Remove user with followId from the follow list")
  public ResponseEntity<List<UserDto>> unfollow(@PathVariable UUID id,
      @PathVariable UUID followId) {
    List<User> followList = service.unfollow(id, followId);
    return ok(mapper.toDto(followList));
  }

  @GetMapping("/{id}/follower")
  @Operation(description = "Get user's follower list")
  public ResponseEntity<List<UserDto>> followerList(@PathVariable UUID id) {
    List<User> followList = service.followerList(id);
    return ok(mapper.toDto(followList));
  }
}
