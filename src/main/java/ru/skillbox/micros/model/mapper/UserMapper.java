package ru.skillbox.micros.model.mapper;

import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.skillbox.micros.model.dto.UserDto;
import ru.skillbox.micros.model.entity.User;

@Mapper(componentModel = "spring")
public interface UserMapper {

  @Mapping(target = "city", ignore = true)
  @Mapping(target = "followList", ignore = true)
  @Mapping(target = "followerList", ignore = true)
  @Mapping(target = "skills", ignore = true)
  @Mapping(target = "patronymic", ignore = true)
  @Mapping(target = "gender", ignore = true)
  @Mapping(target = "birthday", ignore = true)
  @Mapping(target = "avatar", ignore = true)
  @Mapping(target = "bio", ignore = true)
  @Mapping(target = "nickname", ignore = true)
  @Mapping(target = "softDeleted", ignore = true)
  User toEntity(UserDto user);

  UserDto toDto(User user);

  List<UserDto> toDto(List<User> users);
}
