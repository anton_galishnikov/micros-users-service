package ru.skillbox.micros.model.dto;

import java.util.UUID;

public record UserDto(UUID userId, String firstName, String lastName, String email, String phone) {

}
