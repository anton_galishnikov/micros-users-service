package ru.skillbox.micros.model;

public enum SkillType {
  HARD, SOFT, OTHER
}
