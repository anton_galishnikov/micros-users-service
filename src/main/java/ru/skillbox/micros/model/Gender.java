package ru.skillbox.micros.model;

public enum Gender {
  MALE, FEMALE, OTHER
}
