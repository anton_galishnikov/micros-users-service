package ru.skillbox.micros.model.entity;

import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Data
@Entity
@Table(name = "cities")
@SQLDelete(sql = "UPDATE users_schema.cities SET soft_deleted = true WHERE city_id = ?")
@Where(clause = "soft_deleted = false")
public class City {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private UUID cityId;

  private String name;
  private boolean softDeleted;

}
