package ru.skillbox.micros.model.entity;

import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import ru.skillbox.micros.model.SkillType;

@Data
@Entity
@Table(name = "skills")
@SQLDelete(sql = "UPDATE users_schema.skills SET soft_deleted = true WHERE skill_id = ?")
@Where(clause = "soft_deleted = false")
public class Skill {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private UUID skillId;

  private SkillType type;
  private String name;
  private boolean softDeleted;
}
