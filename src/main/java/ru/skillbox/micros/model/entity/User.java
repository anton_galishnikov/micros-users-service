package ru.skillbox.micros.model.entity;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import ru.skillbox.micros.model.Gender;

@Data
@Entity
@Table(name = "users")
@SQLDelete(sql = "UPDATE users_schema.users SET soft_deleted = true WHERE user_id = ?")
@Where(clause = "soft_deleted = false")
public class User {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private UUID userId;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "city_id")
  private City city;

  @OneToMany(fetch = FetchType.LAZY)
  @JoinTable(name = "followers", joinColumns = {
      @JoinColumn(name = "user_id")}, inverseJoinColumns = {@JoinColumn(name = "follower_id")})
  private List<User> followList;

  @OneToMany(fetch = FetchType.LAZY)
  @JoinTable(name = "followers", joinColumns = {
      @JoinColumn(name = "follower_id")}, inverseJoinColumns = {@JoinColumn(name = "user_id")})
  private List<User> followerList;

  @OneToMany(fetch = FetchType.LAZY)
  @JoinTable(name = "user_skills", joinColumns = {
      @JoinColumn(name = "user_id")}, inverseJoinColumns = {@JoinColumn(name = "skill_id")})
  private List<Skill> skills;

  private String firstName;
  private String lastName;
  private String patronymic;
  private Gender gender;
  private LocalDate birthday;
  private String avatar;
  private String bio;
  private String nickname;
  private String email;
  private String phone;
  private boolean softDeleted;
}


