FROM openjdk:17-alpine

WORKDIR /app
COPY build/libs/micros-users-service-*-SNAPSHOT.jar /app
ENV JAVA_OPTS="${JAVA_OPTS}"
ENTRYPOINT java -jar ./micros-users-service-*-SNAPSHOT.jar
CMD $JAVA_OPTS
