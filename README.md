# Micros - Users Service

### Domain Language

#### User

* First name - given name / forename
* Last name - surname / family name
* Patronymic - based on father's given name
* Gender - male/female/other
* Birthday - date of birth
* City - city of residence
* Avatar - link to a picture
* Bio - short text about self
* Nickname - pseudonym
* Skills - list of skills
* Email - primary email
* Phone - primary phone number
* Follow list - list of users followed by current
* Follower list - list of users that are following current

#### City

* Name

#### Skill

* Name
* Type - hard/soft

### Domain Model

See entity relations diagram [here](./puml/domain_model.puml)
